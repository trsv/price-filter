#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include "bitskinsapi.h"
#include "filtermodel.h"
#include "xlsxdocument.h"

#include <QMainWindow>
#include <QStandardItemModel>
#include <QFileDialog>
#include <QLabel>

namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = nullptr);
    ~MainWindow();

private slots:

    void load_geometry();
    void load_settings();
    void load_filters();

    int get_price_for_save_column() const;

    void on_download_button_clicked();
    void on_filter_apply_button_clicked();
    void on_setting_apply_button_clicked();
    void on_edit_button_clicked();
    void on_pushButton_clicked();

    void on_filter_min_price_check_clicked(bool checked);
    void on_filter_average_price_check_clicked(bool checked);
    void on_filter_amount_check_clicked(bool checked);
    void on_filter_trade_amount_check_clicked(bool checked);

    void on_cancel_button_clicked();

    void on_path_finder_button_clicked();

private:
    Ui::MainWindow *ui;

    QLabel * m_all_items_count_label = nullptr;
    QLabel * m_filtered_items_count_label = nullptr;

    BitskinsApi m_bitskins;
    QStandardItemModel * m_model = nullptr;
    FilterModel * m_filter_model = nullptr;

    QModelIndexList m_last_selected_indexes;

    // QWidget interface
protected:
    void closeEvent(QCloseEvent *event);
};

#endif // MAINWINDOW_H
