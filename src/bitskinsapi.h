#ifndef BITSKINSAPI_H
#define BITSKINSAPI_H

#include "totp/qgoogleauth.h"

#include <QJsonObject>
#include <QSettings>
#include <QtNetwork>

class BitskinsApi
{
public:
    BitskinsApi();

    struct Item {

        Item() {}
        Item(const QJsonObject & json) {
            lowest_price = json["lowest_price"].toString().toDouble();
            average_price = json["recent_sales_info"].toObject()["average_price"].toString().toDouble();

            total = json["total_items"].toInt();
            name = json["market_hash_name"].toString();
        }

        double lowest_price = 0.0;
        double average_price = 0.0;
        int total = 0;
        QString name;
    };

    typedef QHash <QString, Item> ItemList;

    void set_api(const QString & key, const QString & secret);
    void set_app_id(const QString & id);

    ItemList get_price_data_for_items_on_sale() const;
    qint64 get_sales_info(const QString &name, const int &page = 1) const;
    QStringList get_popular_skins();

private:

    QNetworkReply * internal_request(const QByteArray & method,
                                     const QNetworkRequest & request,
                                     const QByteArray & data = QByteArray()) const;
    QJsonObject send_request(const QByteArray & method,
                                 const QNetworkRequest & request,
                                 const QByteArray & data = QByteArray()) const;

    QString m_api_key;
    QString m_api_secret;
    QString m_game_id;
};

#endif // BITSKINSAPI_H
