#-------------------------------------------------
#
# Project created by QtCreator 2019-03-17T20:10:35
#
#-------------------------------------------------
QT       += core gui network xlsx

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = price-filter
TEMPLATE = app

DESTDIR = ../build
VERSION = 1.3
QMAKE_TARGET_COPYRIGHT = "trsv"

# The following define makes your compiler emit warnings if you use
# any feature of Qt which has been marked as deprecated (the exact warnings
# depend on your compiler). Please consult the documentation of the
# deprecated API in order to know how to port your code away from it.
DEFINES += QT_DEPRECATED_WARNINGS

# You can also make your code fail to compile if you use deprecated APIs.
# In order to do so, uncomment the following line.
# You can also select to disable deprecated APIs only up to a certain version of Qt.
#DEFINES += QT_DISABLE_DEPRECATED_BEFORE=0x060000    # disables all the APIs deprecated before Qt 6.0.0

CONFIG += c++11

SOURCES += \
        main.cpp \
        mainwindow.cpp \
    totp/base32.cpp \
    totp/hmac.cpp \
    bitskinsapi.cpp \
    filtermodel.cpp

HEADERS += \
        mainwindow.h \
    totp/base32.h \
    totp/hmac.h \
    totp/qgoogleauth.h \
    bitskinsapi.h \
    filtermodel.h

FORMS += \
        mainwindow.ui

# Default rules for deployment.
qnx: target.path = /tmp/$${TARGET}/bin
else: unix:!android: target.path = /opt/$${TARGET}/bin
!isEmpty(target.path): INSTALLS += target

RESOURCES += \
    rsc.qrc
