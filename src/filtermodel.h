#ifndef FILTERMODEL_H
#define FILTERMODEL_H

#include <QSortFilterProxyModel>

class FilterModel : public QSortFilterProxyModel
{
    Q_OBJECT

public:
    FilterModel(QObject * parent = nullptr);

    void set_filter_by_min_price(const bool & value, const double & threshold, const double & t_h);
    void set_filter_by_average_price(const bool & value, const double & threshold, const double & t_h);
    void set_filter_by_amount(const bool & value, const double & threshold, const double & t_h);
    void set_filter_by_trade_amount(const bool & value, const double & threshold, const double & t_h);
    void set_filter_by_popular(const bool &v1, const bool & value);

    void set_stage2_enable(const bool & value);

    // QSortFilterProxyModel interface
protected:
    bool filterAcceptsRow(int source_row, const QModelIndex &source_parent) const;
    bool lessThan(const QModelIndex &source_left, const QModelIndex &source_right) const;

private:
    bool m_is_use_stage2 = false;
    bool m_use_filter_by_popular = true;
    bool m_is_with_popular = false;

    bool m_is_use_filter_by_min_price = false;
    bool m_is_use_filter_by_average_price = false;
    bool m_is_use_filter_by_amount = false;
    bool m_is_use_filter_by_trade_amount = false;

    double m_threshold_by_min_price = 0.0;
    double m_threshold_by_average_price = 0.0;
    double m_threshold_by_amount = 0.0;
    double m_threshold_by_trade_amount = 0.0;

    double m_threshold_by_min_price_high = 0.0;
    double m_threshold_by_average_price_high = 0.0;
    double m_threshold_by_amount_high = 0.0;
    double m_threshold_by_trade_amount_high = 0.0;
};

#endif // FILTERMODEL_H
