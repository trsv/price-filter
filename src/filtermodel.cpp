#include "filtermodel.h"

#include <QDebug>

FilterModel::FilterModel(QObject *parent) :
    QSortFilterProxyModel (parent)
{

}

void FilterModel::set_filter_by_min_price(const bool &value, const double &threshold, const double &t_h)
{
    m_is_use_filter_by_min_price = value;
    m_threshold_by_min_price = threshold;
    m_threshold_by_min_price_high = t_h;

    invalidateFilter();
}

void FilterModel::set_filter_by_average_price(const bool &value, const double & threshold, const double & t_h)
{
    m_is_use_filter_by_average_price = value;
    m_threshold_by_average_price = threshold;
    m_threshold_by_average_price_high = t_h;

    invalidateFilter();
}

void FilterModel::set_filter_by_amount(const bool &value, const double & threshold, const double & t_h)
{
    m_is_use_filter_by_amount = value;
    m_threshold_by_amount = threshold;
    m_threshold_by_amount_high = t_h;

    invalidateFilter();
}

void FilterModel::set_filter_by_trade_amount(const bool &value, const double &threshold, const double &t_h)
{
    m_is_use_filter_by_trade_amount = value;
    m_threshold_by_trade_amount = threshold;
    m_threshold_by_trade_amount_high = t_h;

    invalidateFilter();
}

void FilterModel::set_filter_by_popular(const bool & v1, const bool &value)
{
    m_is_with_popular = value;
    m_use_filter_by_popular = !v1;

    invalidateFilter();
}

void FilterModel::set_stage2_enable(const bool &value)
{
    m_is_use_stage2 = value;

    invalidateFilter();
}

bool FilterModel::filterAcceptsRow(int source_row, const QModelIndex &source_parent) const
{
    const auto name_index =sourceModel()->index(source_row, 0, source_parent);
    const auto min_price_index = sourceModel()->index(source_row, 1, source_parent);
    const auto average_price_index = sourceModel()->index(source_row, 2, source_parent);
    const auto amount_index = sourceModel()->index(source_row, 3, source_parent);
    const auto trade_amount_index = sourceModel()->index(source_row, 4, source_parent);

    const auto is_popular = sourceModel()->data(name_index, Qt::DecorationRole).isValid();
    const auto min_price = sourceModel()->data(min_price_index).toDouble();
    const auto average_price = sourceModel()->data(average_price_index).toDouble();
    const auto amount = sourceModel()->data(amount_index).toDouble();
    const auto trade_amount = sourceModel()->data(trade_amount_index).toDouble();

    const auto valid_min = (m_is_use_filter_by_min_price) ? (min_price >= m_threshold_by_min_price && min_price <= m_threshold_by_min_price_high) : true;
    const auto valid_av = (m_is_use_filter_by_average_price) ? (average_price >= m_threshold_by_average_price && average_price <= m_threshold_by_average_price_high) : true;
    const auto valid_amount = (m_is_use_filter_by_amount) ? (amount >= m_threshold_by_amount && amount <= m_threshold_by_amount_high) : true;
    const auto valid_trade_amount = (m_is_use_stage2 && m_is_use_filter_by_trade_amount) ? (trade_amount >= m_threshold_by_trade_amount && trade_amount <= m_threshold_by_trade_amount_high) : true;
    const auto valid_popular = (m_use_filter_by_popular) ? !(is_popular ^ m_is_with_popular) : true;

    return valid_amount && valid_min && valid_av && valid_trade_amount && valid_popular;
}

bool FilterModel::lessThan(const QModelIndex &source_left, const QModelIndex &source_right) const
{
    if (source_left.column() > 0 && source_right.column() > 0) {
        const auto left = source_left.data().toDouble();
        const auto right = source_right.data().toDouble();

        return left < right;
    } else {
        const auto left = source_left.data().toString();
        const auto right = source_right.data().toString();

        return left < right;
    }

}
