#include "mainwindow.h"
#include "ui_mainwindow.h"

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);

    const auto headers =
            QStringList({"Название", "Мин.\nцена", "Ср.\nцена", "Кол-во", "Кол-во продаж\nза 7 дней"});

    m_model = new QStandardItemModel(0, headers.size(), this);
    m_model->setHorizontalHeaderLabels(headers);

    m_filter_model = new FilterModel(this);
    m_filter_model->setSourceModel(m_model);

    ui->item_list_view->setModel(m_filter_model);
    ui->item_list_view->setSortingEnabled(true);

    load_settings();
    load_filters();

    m_all_items_count_label = new QLabel("0", this);
    m_filtered_items_count_label = new QLabel("0", this);

    ui->statusBar->addWidget(new QLabel("Предметов всего:", this));
    ui->statusBar->addWidget(m_all_items_count_label);

    ui->statusBar->addWidget(new QLabel(", показано:", this));
    ui->statusBar->addWidget(m_filtered_items_count_label);

    ui->item_list_view->horizontalHeader()->setSectionResizeMode(QHeaderView::ResizeMode::Interactive);
    ui->item_list_view->horizontalHeader()->setSectionResizeMode(0, QHeaderView::ResizeMode::Stretch);
    ui->item_list_view->setAlternatingRowColors(true);

    load_geometry();

}

MainWindow::~MainWindow()
{
    delete ui;
}

void MainWindow::load_geometry()
{
    QSettings config(
                qApp->applicationDirPath() + "/resources/config.ini",
                QSettings::Format::IniFormat
                );

    restoreGeometry(config.value("geometry/window/geometry").toByteArray());
    restoreState(config.value("geometry/window/state").toByteArray());

    ui->item_list_view->horizontalHeader()->restoreGeometry(
                config.value("geometry/view/geometry").toByteArray());

    ui->item_list_view->horizontalHeader()->restoreState(
                config.value("geometry/view/state").toByteArray());
}

void MainWindow::load_settings()
{
    QSettings config(
                qApp->applicationDirPath() + "/resources/config.ini",
                QSettings::Format::IniFormat
                );

    const auto key = config.value("api/key").toString();
    const auto secret = config.value("api/secret").toString();
    const auto app_id = config.value("api/app_id").toString();

    ui->api_key_line->setText(key);
    ui->api_secret_line->setText(secret);
    ui->app_id_line->setText(app_id);

    m_bitskins.set_api(key, secret);
    m_bitskins.set_app_id(app_id);

    ui->path_line->setText(config.value("xlsx/path").toString());
    ui->row_start_spin->setValue(config.value("xlsx/start_write_row", 1).toInt());
    ui->column_item_name_spin->setValue(config.value("xlsx/item_name_column", 1).toInt());
    ui->column_item_price_spin->setValue(config.value("xlsx/price_column", 1).toInt());
    ui->column_item_sale_amount_spin->setValue(config.value("xlsx/sale_amount_column", 1).toInt());
    ui->is_price_writtable->setChecked(config.value("xlsx/is_price_writtable", false).toBool());
    ui->is_sale_amount_writtable->setChecked(config.value("xlsx/is_sale_amount_writtable", false).toBool());
}

void MainWindow::load_filters()
{
    QSettings config(
                qApp->applicationDirPath() + "/resources/config.ini",
                QSettings::Format::IniFormat
                );

    const auto is_use_filter_by_min_price = config.value("filter/min_price_use", false).toBool();
    const auto threshold_filter_by_min_price = config.value("filter/min_price_threshold").toString();
    const auto threshold_high_filter_by_min_price = config.value("filter/min_price_threshold_high").toString();

    const auto is_use_filter_by_av_price = config.value("filter/average_price_use", false).toBool();
    const auto threshold_filter_by_av_price = config.value("filter/average_price_threshold").toString();
    const auto threshold_high_filter_by_av_price = config.value("filter/average_price_threshold_high").toString();

    const auto threshold_filter_by_max_price = config.value("filter/max_price_threshold").toString();
    const auto threshold_high_filter_by_max_price = config.value("filter/max_price_threshold_high").toString();

    const auto is_use_filter_by_amount = config.value("filter/amount_use", false).toBool();
    const auto threshold_filter_by_amount = config.value("filter/amount_threshold").toString();
    const auto threshold_high_filter_by_amount = config.value("filter/amount_threshold_high").toString();

    const auto is_use_filter_by_trade_amount = config.value("filter/trade_amount_use", false).toBool();
    const auto threshold_filter_by_trade_amount = config.value("filter/trade_amount_threshold").toString();
    const auto threshold_high_filter_by_trade_amount = config.value("filter/trade_amount_threshold_high").toString();

    const auto mark_all = config.value("filter/mark_all", true).toBool();
    const auto mark_with = config.value("filter/mark_with", false).toBool();
    const auto mark_without = config.value("filter/mark_without", false).toBool();

    ui->filter_min_price_check->setChecked(is_use_filter_by_min_price);
    ui->filter_min_price_line->setText(threshold_filter_by_min_price);
    ui->filter_min_price_line->setEnabled(is_use_filter_by_min_price);
    ui->filter_min_price_high_line->setText(threshold_high_filter_by_min_price);
    ui->filter_min_price_high_line->setEnabled(is_use_filter_by_min_price);

    ui->filter_average_price_check->setChecked(is_use_filter_by_av_price);
    ui->filter_average_price_line->setText(threshold_filter_by_av_price);
    ui->filter_average_price_line->setEnabled(is_use_filter_by_av_price);
    ui->filter_average_price_high_line->setText(threshold_high_filter_by_av_price);
    ui->filter_average_price_high_line->setEnabled(is_use_filter_by_av_price);

    ui->filter_amount_check->setChecked(is_use_filter_by_amount);
    ui->filter_amount_line->setText(threshold_filter_by_amount);
    ui->filter_amount_line->setEnabled(is_use_filter_by_amount);
    ui->filter_amount_high_line->setText(threshold_high_filter_by_amount);
    ui->filter_amount_high_line->setEnabled(is_use_filter_by_amount);

    ui->filter_trade_amount_check->setChecked(is_use_filter_by_trade_amount);
    ui->filter_trade_amount_line->setText(threshold_filter_by_trade_amount);
    ui->filter_trade_amount_line->setEnabled(is_use_filter_by_trade_amount);
    ui->filter_trade_amount_high_line->setText(threshold_high_filter_by_trade_amount);
    ui->filter_trade_amount_high_line->setEnabled(is_use_filter_by_trade_amount);

    ui->is_all->setChecked(mark_all);
    ui->is_with_mark->setChecked(mark_with);
    ui->is_without_mark->setChecked(mark_without);

    m_filter_model->set_filter_by_min_price(
                is_use_filter_by_min_price,
                threshold_filter_by_min_price.toDouble(),
                threshold_high_filter_by_min_price.toDouble());

    m_filter_model->set_filter_by_average_price(
                is_use_filter_by_av_price,
                threshold_filter_by_av_price.toDouble(),
                threshold_high_filter_by_av_price.toDouble());

    m_filter_model->set_filter_by_amount(
                is_use_filter_by_amount,
                threshold_filter_by_amount.toDouble(),
                threshold_high_filter_by_amount.toDouble());

    m_filter_model->set_filter_by_trade_amount(
                is_use_filter_by_trade_amount,
                threshold_filter_by_trade_amount.toDouble(),
                threshold_high_filter_by_trade_amount.toDouble());

    m_filter_model->set_filter_by_popular(mark_all, mark_with);
}

int MainWindow::get_price_for_save_column() const
{
    if (ui->save_min_price_check->isChecked()) {
        return 1;
    } else if (ui->save_av_price_check->isChecked()) {
        return 2;
    } else {
        return 3;
    }

}

void MainWindow::on_download_button_clicked()
{
    m_model->removeRows(0, m_model->rowCount());

    m_filter_model->set_stage2_enable(false);

    const auto popular_skins = m_bitskins.get_popular_skins();
    const auto item_list = m_bitskins.get_price_data_for_items_on_sale();

    for (const auto & item : item_list) {

        const auto row = QList <QStandardItem*>({
            new QStandardItem(item.name),
            new QStandardItem(QString::number(item.lowest_price, 'f', 2)),
            new QStandardItem(QString::number(item.average_price, 'f', 2)),
            new QStandardItem(QString::number(item.total)),
            new QStandardItem()
        });

        if (popular_skins.contains(item.name)) {
            row.first()->setData(QPixmap(":/rsc/icon/icon.png"), Qt::DecorationRole);
        }

        m_model->appendRow(row);

    }

    m_all_items_count_label->setText(QString::number(m_model->rowCount()));
    m_filtered_items_count_label->setText(QString::number(m_filter_model->rowCount()));

    m_filter_model->setDynamicSortFilter(false);

    for (int row = 0; row < m_filter_model->rowCount(); row++) {
        const auto name = m_filter_model->data(m_filter_model->index(row, 0)).toString();
        const auto total = m_bitskins.get_sales_info(name);
        m_filter_model->setData(m_filter_model->index(row, 4), total);
    }

    m_filter_model->set_stage2_enable(true);
    m_filter_model->setDynamicSortFilter(true);

    m_filtered_items_count_label->setText(QString::number(m_filter_model->rowCount()));
}

void MainWindow::on_filter_min_price_check_clicked(bool checked)
{
    ui->filter_min_price_line->setEnabled(checked);
    ui->filter_min_price_high_line->setEnabled(checked);
}

void MainWindow::on_filter_average_price_check_clicked(bool checked)
{
    ui->filter_average_price_line->setEnabled(checked);
    ui->filter_average_price_high_line->setEnabled(checked);
}

void MainWindow::on_filter_amount_check_clicked(bool checked)
{
    ui->filter_amount_line->setEnabled(checked);
    ui->filter_amount_high_line->setEnabled(checked);
}

void MainWindow::on_setting_apply_button_clicked()
{
    const auto key = ui->api_key_line->text();
    const auto secret = ui->api_secret_line->text();
    const auto app_id = ui->app_id_line->text();

    m_bitskins.set_api(key, secret);
    m_bitskins.set_app_id(app_id);

    QSettings config(
                qApp->applicationDirPath() + "/resources/config.ini",
                QSettings::Format::IniFormat
                );

    config.setValue("api/key", key);
    config.setValue("api/secret", secret);
    config.setValue("api/app_id", app_id);

    config.setValue("xlsx/path", ui->path_line->text());
    config.setValue("xlsx/start_write_row", ui->row_start_spin->value());
    config.setValue("xlsx/item_name_column", ui->column_item_name_spin->value());
    config.setValue("xlsx/price_column", ui->column_item_price_spin->value());
    config.setValue("xlsx/sale_amount_column", ui->column_item_sale_amount_spin->value());
    config.setValue("xlsx/is_price_writtable", ui->is_price_writtable->isChecked());
    config.setValue("xlsx/is_sale_amount_writtable", ui->is_sale_amount_writtable->isChecked());
}

void MainWindow::on_filter_apply_button_clicked()
{
    const auto is_use_filter_by_min_price = ui->filter_min_price_check->isChecked();
    const auto threshold_filter_by_min_price = ui->filter_min_price_line->text();
    const auto threshold_high_filter_by_min_price = ui->filter_min_price_high_line->text();

    const auto is_use_filter_by_av_price = ui->filter_average_price_check->isChecked();
    const auto threshold_filter_by_av_price = ui->filter_average_price_line->text();
    const auto threshold_high_filter_by_av_price = ui->filter_average_price_high_line->text();

    const auto is_use_filter_by_amount = ui->filter_amount_check->isChecked();
    const auto threshold_filter_by_amount = ui->filter_amount_line->text();
    const auto threshold_high_filter_by_amount = ui->filter_amount_high_line->text();

    const auto is_use_filter_by_trade_amount = ui->filter_trade_amount_check->isChecked();
    const auto threshold_filter_by_trade_amount = ui->filter_trade_amount_line->text();
    const auto threshold_high_filter_trade_by_amount = ui->filter_trade_amount_high_line->text();

    m_filter_model->set_filter_by_min_price(
                is_use_filter_by_min_price,
                threshold_filter_by_min_price.toDouble(),
                threshold_high_filter_by_min_price.toDouble());

    m_filter_model->set_filter_by_average_price(
                is_use_filter_by_av_price,
                threshold_filter_by_av_price.toDouble(),
                threshold_high_filter_by_av_price.toDouble());

    m_filter_model->set_filter_by_amount(
                is_use_filter_by_amount,
                threshold_filter_by_amount.toDouble(),
                threshold_high_filter_by_amount.toDouble());

    m_filter_model->set_filter_by_trade_amount(
                is_use_filter_by_trade_amount,
                threshold_filter_by_trade_amount.toDouble(),
                threshold_high_filter_trade_by_amount.toDouble());

    m_filter_model->set_filter_by_popular(ui->is_all->isChecked(), ui->is_with_mark->isChecked());

    QSettings config(
                qApp->applicationDirPath() + "/resources/config.ini",
                QSettings::Format::IniFormat
                );

    config.setValue("filter/min_price_use", is_use_filter_by_min_price);
    config.setValue("filter/min_price_threshold", threshold_filter_by_min_price);
    config.setValue("filter/min_price_threshold_high", threshold_high_filter_by_min_price);

    config.setValue("filter/average_price_use", is_use_filter_by_av_price);
    config.setValue("filter/average_price_threshold", threshold_filter_by_av_price);
    config.setValue("filter/average_price_threshold_high", threshold_high_filter_by_av_price);

    config.setValue("filter/amount_use", is_use_filter_by_amount);
    config.setValue("filter/amount_threshold", threshold_filter_by_amount);
    config.setValue("filter/amount_threshold_high", threshold_high_filter_by_amount);

    config.setValue("filter/trade_amount_use", is_use_filter_by_trade_amount);
    config.setValue("filter/trade_amount_threshold", threshold_filter_by_trade_amount);
    config.setValue("filter/trade_amount_threshold_high", threshold_high_filter_trade_by_amount);

    config.setValue("filter/mark_all", ui->is_all->isChecked());
    config.setValue("filter/mark_with", ui->is_with_mark->isChecked());
    config.setValue("filter/mark_without", ui->is_without_mark->isChecked());

    m_filtered_items_count_label->setText(QString::number(m_filter_model->rowCount()));
}

void MainWindow::on_edit_button_clicked()
{
    const auto value = 1.0 + (ui->edit_value_line->text().toDouble() / 100.0);
    const auto selected_list = ui->item_list_view->selectionModel()->selectedIndexes();

    m_last_selected_indexes = selected_list;

    for (const auto & select : selected_list) {

        if (select.column() > 0) {

            const auto old_value = select.data().toDouble();
            const auto new_value = old_value * value;

            m_filter_model->setData(select, QString::number(old_value, 'f', 2), Qt::UserRole);
            m_filter_model->setData(select, QString::number(new_value, 'f', 2));
        }

    }
}

void MainWindow::on_filter_trade_amount_check_clicked(bool checked)
{
    ui->filter_trade_amount_line->setEnabled(checked);
    ui->filter_trade_amount_high_line->setEnabled(checked);
}

void MainWindow::on_pushButton_clicked()
{
    const auto path = ui->path_line->text();

    if (path.isEmpty()) {
        return;
    }

    const auto price_column = get_price_for_save_column();
    const auto column_in_document_for_item_name = ui->column_item_name_spin->value();
    const auto column_in_document_for_item_price = ui->column_item_price_spin->value();
    const auto column_in_document_for_item_sale_amount = ui->column_item_sale_amount_spin->value();

    const auto is_price_writtable = ui->is_price_writtable->isChecked();
    const auto is_sale_amount_writtable = ui->is_sale_amount_writtable->isChecked();

    QFileInfo info(path);

    const auto is_exist = info.exists();

    QXlsx::Document d(path, this);

    if (is_exist) {
        QHash <QString, int> row_map;

        const auto last_row = d.dimension().lastRow();
        for (int row = ui->row_start_spin->value(); row <= last_row; row++) {
            row_map.insert(d.read(row, column_in_document_for_item_name).toString(), row);
        }

        for (int item_row = 0; item_row < m_filter_model->rowCount(); ++item_row) {

            const auto item_name = m_filter_model->data(m_filter_model->index(item_row, 0)).toString();

            if (!row_map.contains(item_name)) {
                continue;
            }

            const auto row_in_document = row_map.value(item_name);

            if (is_price_writtable) {
                d.write(row_in_document,
                        column_in_document_for_item_price,
                        m_filter_model->data(m_filter_model->index(item_row, price_column)));
            }

            if (is_sale_amount_writtable) {
                d.write(row_in_document,
                        column_in_document_for_item_sale_amount,
                        m_filter_model->data(m_filter_model->index(item_row, 4)));
            }

        }

    } else {

        for (int item_row = 0, row_in_document = ui->row_start_spin->value(); item_row < m_filter_model->rowCount(); ++item_row, ++row_in_document) {

            d.write(row_in_document,
                    column_in_document_for_item_name,
                    m_filter_model->data(m_filter_model->index(item_row, 0)));

            if (is_price_writtable) {
                d.write(row_in_document,
                        column_in_document_for_item_price,
                        m_filter_model->data(m_filter_model->index(item_row, price_column)));
            }

            if (is_sale_amount_writtable) {
                d.write(row_in_document,
                        column_in_document_for_item_sale_amount,
                        m_filter_model->data(m_filter_model->index(item_row, 4)));
            }

        }
    }


    if (is_exist) {
        d.save();
    } else {

        QDir().mkpath(info.absolutePath());

        auto editted_path = path;
        editted_path = editted_path.insert(
                    editted_path.lastIndexOf(".xlsx"),
                    "-" + QDate::currentDate().toString(Qt::DateFormat::SystemLocaleShortDate));

        d.saveAs(editted_path);
    }
}

void MainWindow::on_cancel_button_clicked()
{
    for (const auto & select : m_last_selected_indexes) {

        if (select.column() > 0) {
            const auto old_value = m_filter_model->data(select, Qt::UserRole);
            m_filter_model->setData(select, old_value);
        }

    }
}

void MainWindow::on_path_finder_button_clicked()
{
    const auto path = QFileDialog::getOpenFileName(this, "Find xlsx file for write",
                                                   QApplication::applicationDirPath(),
                                                   "xlsx files (*.xlsx)");
    if (!path.isEmpty()) {
        ui->path_line->setText(path);
    }
}

void MainWindow::closeEvent(QCloseEvent *event)
{
    QSettings config(
                qApp->applicationDirPath() + "/resources/config.ini",
                QSettings::Format::IniFormat
                );

    config.setValue("geometry/window/geometry", saveGeometry());
    config.setValue("geometry/window/state", saveState());

    config.setValue("geometry/view/geometry", ui->item_list_view->horizontalHeader()->saveGeometry());
    config.setValue("geometry/view/state", ui->item_list_view->horizontalHeader()->saveState());

    QMainWindow::closeEvent(event);
}
