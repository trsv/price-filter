#include "bitskinsapi.h"

BitskinsApi::BitskinsApi()
{

}

void BitskinsApi::set_api(const QString &key, const QString &secret)
{
    m_api_key = key;
    m_api_secret = secret;
}

void BitskinsApi::set_app_id(const QString &id)
{
    m_game_id = id;
}

BitskinsApi::ItemList BitskinsApi::get_price_data_for_items_on_sale() const
{
    static const auto url_pattern =
            QString("https://bitskins.com/api/v1/get_price_data_for_items_on_sale/?api_key=%1&code=%2&app_id=%3");

    const auto code = QGoogleAuth::generatePin(m_api_secret.toUtf8());
    const auto url = url_pattern.arg(m_api_key, code, m_game_id);

    const auto response = send_request("GET", QNetworkRequest(QUrl(url)));
    const auto items = response["data"].toObject()["items"].toArray();

    ItemList list;
    for (const auto & item : items) {
        const Item i = item.toObject();
        list.insert(i.name, i);
    }

    return list;
}

qint64 BitskinsApi::get_sales_info(const QString & name, const int &page) const
{
    static const auto url_pattern =
            QString("https://bitskins.com/api/v1/get_sales_info/?api_key=%1&code=%2&app_id=%3&page=%4&market_hash_name=%5");

    const auto code = QGoogleAuth::generatePin(m_api_secret.toUtf8());
    const auto url = url_pattern.arg(m_api_key, code, m_game_id, QString::number(page), name);

    auto response = send_request("GET", QNetworkRequest(QUrl(url)));

    while (response.isEmpty()) {
        QThread::sleep(2);
        response = send_request("GET", QNetworkRequest(QUrl(url)));
    }

    const auto sales = response["data"].toObject()["sales"].toArray();

    const auto now_day = QDateTime::currentDateTime();
    auto total_at_7day = 0;

    for (const auto & sale_raw : sales) {
        const auto trade_date = QDateTime::fromTime_t(sale_raw.toObject()["sold_at"].toVariant().toUInt());
        const auto days = trade_date.daysTo(now_day);

        if (days > 7) {
            return total_at_7day;
        }

        ++total_at_7day;
    }

    return (page < 5) ? total_at_7day + get_sales_info(name, page + 1) : total_at_7day;
}

QStringList BitskinsApi::get_popular_skins()
{
    static const auto url_pattern = QString("https://cs.money/popular_skins?appid=%1");

    const auto url = url_pattern.arg(m_game_id);

    auto reply = internal_request("GET", QNetworkRequest(QUrl(url)));
    if (reply->error() == QNetworkReply::NoError) {

        QJsonParseError error;
        const auto json = QJsonDocument::fromJson(reply->readAll(), &error);
        reply->manager()->deleteLater();
        reply->deleteLater();

        if (error.error == QJsonParseError::NoError) {
            return qvariant_cast <QStringList> (json.array().toVariantList());
        } else {
            qWarning() << "[bitskins api] Get popular skins. Json parse error" << error.errorString();
            return QStringList();
        }

    } else {
        qWarning() << "[bitskins api] Get popular slins. Error"
                   << reply->request().url().toString()
                   << reply->errorString()
                   << reply->readAll();
        reply->manager()->deleteLater();
        reply->deleteLater();
        return QStringList();
    }
}

QNetworkReply *BitskinsApi::internal_request(const QByteArray &method, const QNetworkRequest &request, const QByteArray &data) const
{
    auto network = new QNetworkAccessManager;
    auto reply = network->sendCustomRequest(request, method, data);

    QEventLoop loop;
    QObject::connect(reply, &QNetworkReply::finished, &loop, &QEventLoop::quit);
    loop.exec();

    return reply;
}

QJsonObject BitskinsApi::send_request(const QByteArray &method,
                                      const QNetworkRequest &request,
                                      const QByteArray &data) const
{
    auto reply = internal_request(method, request, data);

    if (reply->error() == QNetworkReply::NoError) {

        QJsonParseError error;
        const auto json = QJsonDocument::fromJson(reply->readAll(), &error);
        reply->manager()->deleteLater();
        reply->deleteLater();

        if (error.error == QJsonParseError::NoError) {
            return json.object();
        } else {
            qWarning() << "[bitskins api] Json parse error" << error.errorString();
            return QJsonObject();
        }

    } else {
        qWarning() << "[bitskins api] Error"
                   << request.url().toString()
                   << reply->errorString()
                   << reply->readAll();
        reply->manager()->deleteLater();
        reply->deleteLater();
        return QJsonObject();
    }
}
